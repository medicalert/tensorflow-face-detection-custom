#!/bin/bash

for mycommand in python3; do
  if ! type "$mycommand" >& /dev/null; then
    echo "This script requires that $mycommand is installed."
    echo "If you don't have $mycommand, install it before running"
    echo "this script.  If you do have $mycommand, check that it"
    echo "is in your path."
    exit 1
  fi
done

if ! python3 ~/Documents/tensorflow-face-detection/inference.py; then
  echo "Tensorflow failed"
  exit 1
fi

exit 0
