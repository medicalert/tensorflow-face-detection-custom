import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os, sys
import subprocess

#command = "./darknet"
#param = "./darknet detector test cfg/face.data yolo-face.cfg yolo-face_500000.weights ./images/crowd6.jpg"
#param = "detector test cfg/face.data yolo-face.cfg yolo-face_500000.weights ./images/crowd6.jpg"
param = "python3"
#ml = [param, "detector", "test", "cfg/face.data", "yolo-face.cfg", "yolo-face_500000.weights", "./images/crowd6.jpg"]
ml = [param, "inference.py"]

for x in range(10):
    p = subprocess.call(ml, stdout=subprocess.PIPE)