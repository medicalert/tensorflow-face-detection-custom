#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
from scipy import misc
import time
import detect
import os
import sys
import scipy.io as sio
from tqdm import tqdm

if(len(sys.argv) < 3):
	print("Incorrect arguments\nCorrect argument format: <program_nam> <WIDER image path> <WIDER mat file> <WIDER category (optional)>\n")
	exit(-1)

if(sys.argv[1] == 'old'):
	WF_DIR = "/home/vj/Documents/WIDER_val/images/"
else:
	WF_DIR = sys.argv[1]
#WF_SPEC = "{0}21--Festival".format(WF_DIR)

if(len(sys.argv) == 4):
    WF_SPEC = "{0}{1}".format(WF_DIR, sys.argv[2])
else:
    print("Retrieving mat file...")
    WF_MAT = sio.loadmat(sys.argv[2])
    WF_R_DIR = "/home/vj/Documents/WIDER_results/mobilenet_ssd/"
    event_list = WF_MAT['event_list']
del WF_MAT

#event in event_list
for event in tqdm(event_list, ascii=True, desc='Overall Progress'):
    #make directory in detection folder:
    WF_SPEC = event[0][0]
    try:
        os.mkdir("{0}{1}".format(WF_R_DIR, WF_SPEC))
    except FileExistsError as e:
        pass
        #print("Event catergory already exists, continuing...")
    
    #get the images from event_list in WIDER_VAL
    img_list = os.listdir(WF_DIR+WF_SPEC)
    for img_path in tqdm(img_list, ascii=True, desc='  Event Progress'):
        result = detect.main(WF_DIR + WF_SPEC + "/" + img_path)
        bounding_boxes = result[0]
        confidence = result[1]
        with open(WF_R_DIR + WF_SPEC + "/" + img_path[:-4]+".txt", 'w') as file:
            file.write("{0}\n{1}\n".format(img_path,len(result[0])))
            #assuming bounding boxes and confidence are same size:
            i = 0
            for bounding_box in bounding_boxes:
                #xmin, ymin, xmax, ymax
                width = bounding_box[2] - bounding_box[0]
                height = bounding_box[3] - bounding_box[1]
                file.write("{0} {1} {2} {3} {4}\n".format(bounding_box[0], bounding_box[1], width, height, confidence[i]))
                i += 1